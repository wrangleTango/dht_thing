import time
import os
import board
import wifi
import socketpool
import ssl

import adafruit_dht
import digitalio
import adafruit_requests
from adafruit_io.adafruit_io import IO_HTTP, AdafruitIO_RequestError


# Temperature and humidity sensor on GP20.
dht = adafruit_dht.DHT22(board.GP20)

# Indicator LED on GP16.
indicator = digitalio.DigitalInOut(board.GP16)
indicator.direction = digitalio.Direction.OUTPUT

def connection():
    wifi.radio.connect(os.getenv("CIRCUITPY_WIFI_SSID"), os.getenv("CIRCUITPY_WIFI_PASSWORD"))

# Access wifi per settings.toml file. Create pool object.
try:
    connection()
except:
    connection()
    
pool = socketpool.SocketPool(wifi.radio)

print(f"WiFi connected to wireless network {os.getenv('CIRCUITPY_WIFI_SSID')}")
print(f"at {wifi.radio.ipv4_address}")

# Connect to Adafruit IO with credentials from settingsl.toml, create IO session.
aio_username = os.getenv("aio_username")
aio_key = os.getenv("IO_KEY")
requests = adafruit_requests.Session(pool, ssl.create_default_context())
io = IO_HTTP(aio_username, aio_key, requests)
print("Connected to io")

dht_thing_temp = io.get_feed("ham-temp")
dht_thing_humid = io.get_feed("ham-humidity")

# Read sensor values, update IO feed, blink LED and repeat every fifteen seconds.
while True:
    print(dht.temperature, " ", dht.humidity)
    io.send_data("ham-humidity", dht.humidity)
    io.send_data("ham-temp", dht.temperature)
    indicator.value = True
    time.sleep(1)
    indicator.value = False
    time.sleep(15)
